class LinksController < ApplicationController

	def index
		@link = Link.new
		@links = Link.paginate(page: params[:page])
	end

	def create
		@link = Link.new(link_params)
		if @link.save
			redirect_to root_url
		else
			redirect_to root_url
		end
	end

	private 

	def link_params
		params.require(:link).permit(:url)
	end
end

