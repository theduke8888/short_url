class Link < ActiveRecord::Base
	before_save :set_short_url
	default_scope -> { order(created_at: :desc)}

	validates :url, presence: true

	private 
		
		def set_short_url() 
			self.short_url = generate_short_url
		end
		
		# generate token by using SecureRandom 
		def generate_short_url
			loop do
				url = SecureRandom.hex(2) + ".gl"
				if !Link.where(short_url: url).exists? 
					break url
				end

			end
		end
end
