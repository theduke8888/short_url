class AddShortLinkToLinks < ActiveRecord::Migration
  def change
  	add_column :links, :short_url, :string
  	add_index :links, :short_url, unique: true
  end
end
